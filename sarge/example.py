# -*- coding:utf-8 -*-
from sarge import capture_stdout


def example():
    """
    コマンド実行のために必要なパッケージ
    $ sudo apt-get install fortune-mod cowsay
    """
    p = capture_stdout('fortune|cowthink')
    print(p.returncode)
    print(p.commands)
    print(p.returncodes)
    print(p.stdout.text)


if __name__ == '__main__':
    print('--> example()')
    example()
