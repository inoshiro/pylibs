# -*- coding: utf-8 -*-
import logging
from logging import getLogger


logging.basicConfig(level=logging.WARNING)


def example():
    # ログレベルの閾値はデフォルト(warning以上しか出力されないはず)
    logger = getLogger(__name__)
    print(logger.name)
    logger.critical('critical')
    logger.error('error')
    logger.warning('warn')
    logger.info('info')
    logger.debug('debug')

    print('Level WARNING:', logger.isEnabledFor(logging.WARNING))
    print('Level DEBUG:', logger.isEnabledFor(logging.DEBUG))

    # ログレベルの閾値をDEBUGにする(全部出力される)
    logger.setLevel(logging.DEBUG)
    print("\n>>> set logger level to DEBUG\n")
    logger.critical('critical')
    logger.error('error')
    logger.warning('warn')
    logger.info('info')
    logger.debug('debug')

    print('Level WARNING:', logger.isEnabledFor(logging.WARNING))
    print('Level DEBUG:', logger.isEnabledFor(logging.DEBUG))

    print("\n>>> create new logger\n")
    logger_named = getLogger('hoge.fuga.piyo')
    print('logger name is {0}'.format(logger_named.name))
    # exist name
    print('child logger is {0}'.format(logging.getLogger('hoge').getChild('fuga').name))
    # not exist name
    print('child logger is {0}'.format(logging.getLogger('hoge').getChild('ham').name))


if __name__ == '__main__':
    example()