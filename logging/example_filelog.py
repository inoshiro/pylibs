import logging


def example():
    """
    ログの出力先をファイルにする
    """
    logging.basicConfig(filename='example.log', level=logging.DEBUG)
    logging.debug('debugging')
    logging.info('info')
    logging.warning('warning')


if __name__ == '__main__':
    example()
