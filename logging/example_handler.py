# -*- coding: utf-8 -*-
import logging
import io


def example_stream():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # formatterを定義する
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # StreamHandlerはStreamを受け取れるので、StringIOを渡してみる
    stream = io.StringIO()
    handler_stream = logging.StreamHandler(stream)
    handler_stream.setFormatter(formatter)
    handler_stream.setLevel(logging.WARNING)
    logger.addHandler(handler_stream)

    # 複数のhandlerもいける
    f = open('/tmp/fugafuga.txt', 'w+')
    handler_file = logging.StreamHandler(f)
    handler_file.setFormatter(formatter)
    logger.addHandler(handler_file)

    logger.debug('hogehoge')
    logger.info('hogehoge')
    logger.warning('hogehoge')
    logger.error('hogehoge')

    print('>> print content in stream')
    print(stream.getvalue())

    f.seek(0)
    print('>> print content in file')
    print(f.read())
    f.close()
