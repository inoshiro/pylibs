# -*- coding:utf-8 -*-
import logging


def example():
    """
    デフォルト設定の場合
    """
    logging.error('error')
    logging.warning('warning!')
    logging.info('info')


def example_with_variables():
    """
    ログメッセージに変数を埋め込む場合
    """
    logging.warning('%s and %s', 'hoge', 'fuga')


if __name__ == '__main__':
    example()
    example_with_variables()
