# -*- coding: utf-8 -*-
import logging


def example():
    """
    ログ全体のフォーマットを設定する
    """
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
    logging.warning('hogefuga')
    logging.error('die')


if __name__ == '__main__':
    example()