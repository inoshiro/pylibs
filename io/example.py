# -*- coding: utf-8 -*-
import io
from timeit import Timer


def example():
    snip = "hogehoge"


def build_with_stringio():
    stream = io.StringIO()
    s = "hogefugaababadfadfadfadfasdfasdflajdflajfdldfabababab"
    for i in range(100000):
        stream.write(s)


def build_with_string():
    string = ""
    s = "hogefugaababadfadfadfadfasdfasdflajdflajfdldfabababab"
    for i in range(100000):
        string = string + s


def build_with_binstringio():
    stream = io.BytesIO()
    s = b"hogefugaababadfadfadfadfasdfasdflajdflajfdldfabababab"
    for i in range(100000):
        stream.write(s)


def build_with_bytearray():
    buf = bytearray()
    s = b"hogefugaababadfadfadfadfasdfasdflajdflajfdldfabababab"
    for i in range(100000):
        buf += s


if __name__ == '__main__':
    example()
    print(">>> StringIO")
    print(Timer('build_with_stringio()', 'from __main__ import build_with_stringio').timeit(number=100))
    print(">>> BytesIO")
    print(Timer('build_with_binstringio()', 'from __main__ import build_with_binstringio').timeit(number=100))
    print(">>> str")
    print(Timer('build_with_string()', 'from __main__ import build_with_string').timeit(number=100))
    print(">>> bytearray")
    print(Timer('build_with_bytearray()', 'from __main__ import build_with_bytearray').timeit(number=100))
