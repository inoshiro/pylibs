import io
import unittest
from unittest.mock import patch


def example():
    pass


def output_to_file(f):
    f.write('hogehoge')


def print_string():
    print('hogehoge')


class TestFileLikeBehavior(unittest.TestCase):

    def test_file(self):
        f = io.StringIO()
        output_to_file(f)
        assert f.getvalue() == 'hogehoge'

    @patch('sys.stdout', new_callable=io.StringIO)
    def test_stdout(self, mocked_object):
        print_string()
        assert mocked_object.getvalue() == 'hogehoge\n'


if __name__ == '__main__':
    unittest.main()
