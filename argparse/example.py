# -*- coding: utf-8 -*-
import argparse

def example():
    parser = argparse.ArgumentParser(
        prog='HogeProgram',
        usage='HogeProgram --help',
        description='Example command',
        epilog='this is argparse example command',
        parents=[],
        formatter_class=argparse.HelpFormatter,
        prefix_chars='-',
        fromfile_prefix_chars=None,
        argument_default=None,
        conflict_handler='error',
        add_help=True
    )

    parser.parse_args(['--help'])


if __name__ == '__main__':
    example()