# -*- coding: utf-8 -*-
import os


def example():
    print(">>> os.ctermid()")
    print(os.ctermid())


if __name__ == '__main__':
    example()