# -*- coding: utf-8 -*-
import smtplib

from email.message import EmailMessage
from email.headerregistry import Address


def example():
    msg = EmailMessage()
    msg['Subject'] = "this is subject"
    me = Address("Kenjiro Kosaka", "inoshirou@gmail.com")
    msg['From'] = me
    msg['To'] = (me, )

    msg.set_content("""\
hoge
fuga
piyo
    """)

    print(msg)

    with smtplib.SMTP('localhost') as s:
        s.send_message(msg)


if __name__ == '__main__':
    example()